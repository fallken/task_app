const Board = require('../models/BoardModel');
const Card = require('../models/CardModel');

const BoardController = {
    create:async (req,res)=>{
        try {
            let board = await Card.create({
                ...req.body,
                user:req.user._id
            });
            res.status(200).send(board);
        } catch (e) {
            console.log(e)
            res.status( e.code ? e.code : 400).json({error:e.message});
        }
    },
    all:async(req,res)=>{
        try {
            const boards = await Board.find({user:req.user._id});
            res.status(200).send(boards);
        } catch (e) {
            console.log(e)
            res.status( e.code ? e.code : 400).json({error:e.message});
        }
    },
    show:async(req,res)=>{
        try {
            const board = await Board.find({_id:req.params.id,user:req.user._id});
            res.status(200).send(board);
        } catch (e) {
            console.log(e)
            res.status( e.code ? e.code : 400).json({error:e.message});
        }
    },
    update:async(req,res)=>{
        try {
            let board = await Board.findOneAndUpdate({_id:req.params.id,user:req.user._id},{
                ...req.body
            });
            res.status(200).send(board);
        } catch (e) {
            console.log(e)
            res.status( e.code ? e.code : 400).json({error:e.message});
        }
    },
    delete:async(req,res)=>{
        try {
            await Board.findOneAndDelete({_id:req.params.id,user:req.user._id});
            res.status(200).send('successfully deleted board');
        } catch (e) {
            console.log(e)
            res.status( e.code ? e.code : 400).json({error:e.message});
        }
    }
}

module.exports = BoardController;