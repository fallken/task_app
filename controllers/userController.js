const User = require('../models/UserModel');

const UserController = {
    register:async (req,res)=>{
        try{
            const user = new User(req.body);
            await user.save()
            const token = await user.generateAuthToken()
            res.status(200).send({ user, token })
        }
        catch(e){
            console.log(e)
            res.status(400).json({error:e.message});
        }
    },
    login:async (req,res)=>{
        try{
            const { email, password } = req.body
            const user = await User.findByCredentials(email, password);
            if (!user) {
                return res.status(401).send({error: 'auth failed'})
            }
            const token = await user.generateAuthToken()
            res.send({ user, token })
        }
        catch(e){
            console.log(e)
            res.status( 400).json({error:e.message});
        }
    },
    logout:async (req,res)=>{
        try {
            req.user.tokens = req.user.tokens.filter((token) => {
                return token.token != req.token
            })
            await req.user.save()
            res.send('logout was a success');
        } catch (e) {
            console.log(e)
            res.status( 500).json({error:e.message});
        }
    },
    logoutAllDevices:async (req,res)=>{
        try {
            req.user.tokens.splice(0, req.user.tokens.length)
            await req.user.save()
            res.send('logged out from all accounts');
        } catch (e) {
            console.log(e)
            res.status( 500).json({error:e.message});
        }
    },
    me:async (req,res)=>{
        try{
            res.send(req.user);
        }
        catch(e){
            console.log(e)
            res.status( 500).json({error:e.message});
        }
    }
}

module.exports = UserController;