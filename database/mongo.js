const mongoose = require("mongoose");
const dbPath = process.env.MONGODB_URL || "mongodb://localhost/tasks";
mongoose.connect(dbPath, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify:false
});
const db = mongoose.connection;
db.on("error", () => {
    console.log("connection error");
});
db.once("open", () => {
    console.log("connected");
});
module.exports = mongoose;
