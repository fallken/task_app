const jwt = require('jsonwebtoken')
const User = require('../models/UserModel')

const auth = async(req, res, next) => {
    try {
        const auth_header = req.header('Authorization');
        if(!auth_header)throw new Error();
        const token = auth_header.replace('Bearer ', '');
        const data = jwt.verify(token, process.env.JWT_KEY);
        const user = await User.findOne({ _id: data._id, 'tokens.token': token }).select(['-password']);
        if (!user) {
            throw new Error();
        }
        req.user = user
        req.token = token
        next()
    } catch (e) {
        console.log('we got the error here ');
        res.status(e.code ? e.code : 500).send({ error: 'unauthorized access fuck off !!' })
    }
}
module.exports = auth