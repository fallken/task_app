const mongoose = require("../database/mongo");
const Schema = mongoose.Schema;

const schema = {
    title: { type: mongoose.SchemaTypes.String, required: true },
    board:{
        type:Schema.Types.ObjectId,
        ref:"Board",
        required:true
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User",
        required:true
    }
};
const collectionName = "BoardColumn";
const BoardColumnSchema = mongoose.Schema(schema);
const BoardColumn = mongoose.model(collectionName, BoardColumnSchema);
module.exports = BoardColumn;

