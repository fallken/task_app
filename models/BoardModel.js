const mongoose = require("../database/mongo");
const Schema = mongoose.Schema;

const schema = {
    title: { type: mongoose.SchemaTypes.String, required: true },
    body: { type: mongoose.SchemaTypes.String, required: true },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User"
    },
    columns:[{
        type:Schema.Types.ObjectId,
        ref:"BoardColumn"
    }],
    cards:[{
        type:Schema.Types.ObjectId,
        ref:"Card"
    }]
};
const collectionName = "Board";
const CardSchema = mongoose.Schema(schema);
const Card = mongoose.model(collectionName, CardSchema);
module.exports = Card;

