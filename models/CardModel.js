const mongoose = require("../database/mongo");
const Schema = mongoose.Schema;

const schema = {
    title: { type: mongoose.SchemaTypes.String, required: true },
    body: { type: mongoose.SchemaTypes.String, required: true },
    column:{
        type:Schema.Types.ObjectId,
        ref:"BoardColumn",
        required:true
    },
    board:{
        type:Schema.Types.ObjectId,
        ref:"Board",
        required:true
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User"
    }
};
const collectionName = "Card";
const CardSchema = mongoose.Schema(schema);
const Card = mongoose.model(collectionName, CardSchema);
module.exports = Card;
