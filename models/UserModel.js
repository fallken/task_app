const mongoose = require("../database/mongo");
const validator = require("validator");
const bcrypt  = require("bcrypt");
const jwt  = require("jsonwebtoken");
const Schema = mongoose.Schema;

const schema = {
    name: { 
        type: mongoose.SchemaTypes.String,
        required: true,
        trim: true,
        unique: true,
        lowercase: true
     },
    email: { 
        type: mongoose.SchemaTypes.String, required: true,
        validate: value => {
            if (!validator.isEmail(value)) {
                throw new Error({error: 'Invalid Email address'})
            }
        } },
    password: { 
        type: mongoose.SchemaTypes.String, 
        required: true, 
        minLength: 7
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    Boards:[{
        type:Schema.Types.ObjectId,
        ref:"Board"
    }]
};


const collectionName = "User";
const userSchema = mongoose.Schema(schema);


userSchema.pre('save', async function (next) {
    // Hash the password before saving the user model
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    next()
})

userSchema.methods.generateAuthToken = async function() {
    // Generate an auth token for the user
    const user = this
    const token = jwt.sign({_id: user._id}, process.env.JWT_KEY)
    user.tokens = user.tokens.concat({token})
    await user.save()
    return token
}

userSchema.statics.findByCredentials = async (email, password) => {
    // Search for a user by email and password.
    const user = await User.findOne({ email} )
    if (!user) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password)
    if (!isPasswordMatch) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    return user
}



const User = mongoose.model(collectionName, userSchema);
module.exports = User;
