const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const boardController = require('../controllers/boardController');
//TODO:use joi for validating inputs
router.post('/', auth, boardController.all);
router.get('/:id', auth, boardController.show);
router.post('/add', auth, boardController.create);
router.put('/:id', auth, boardController.update);
router.delete('/:id', auth, boardController.delete);

module.exports = router;
