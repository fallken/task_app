const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const ColumnController = require('../controllers/BoardColumnController');
//TODO:use joi for validating inputs
router.post('/', auth, ColumnController.all);
router.get('/:id', auth, ColumnController.show);
router.post('/add', auth, ColumnController.create);
router.put('/:column_id', auth, ColumnController.update);
router.delete('/:id', auth, ColumnController.delete);
module.exports = router;