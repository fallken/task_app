const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const CardController = require('../controllers/cardController');
//TODO:use joi for validating inputs
router.post('/', auth, CardController.all);
router.get('/:id', auth, CardController.show);
router.post('/add', auth, CardController.create);
router.put('/:id', auth, CardController.update);
router.delete('/:id', auth, CardController.delete);
module.exports = router;