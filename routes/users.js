const  express = require('express');
const  router = express.Router();
const  auth = require('../middleware/auth');
const UserController = require('../controllers/userController');
//TODO:use joi for validating inputs
router.post('/',UserController.register);
router.post('/login',UserController.login);
router.get('/me',auth,UserController.me);
router.get('/me/logout',auth,UserController.logout);
router.get('/me/logoutall',auth,UserController.logoutAllDevices);


module.exports = router;
